import WebKit

struct Cookie {
    var name: String
    var value: String
}

let gcmMessageIDKey = "00000000000" // update this with actual ID if using Firebase 

// URL for first launch
let rootUrl = URL(string: "https://betawebpos.izipoint.by/?utm_source=homescreen")!

// allowed origin is for what we are sticking to pwa domain
// This should also appear in Info.plist
let allowedOrigins: [String] = [
    "appleid.apple.com",
    "appleid.cdn-apple.com",
    "accounts.google.com",
    "myaccount.google.com",
    "accounts.youtube.com",
    "play.google.com",
    "www.gstatic.com",
    "signaler-pa.googleapis.com",
    "gds.google.com",
    "www.googletagmanager.com",
    "www.google-analytics.com",
    "lh3.googleusercontent.com",
    "ssl.gstatic.com",
    "fonts.gstatic.com",
    "analytics.google.com",
    "stats.g.doubleclick.net",
    "ipapi.co",
    "acs.cloudpayments.ru",
    "checkout.cloudpayments.ru",
    "betaapi.izipoint.io",
    "betawebpos.izipoint.by",
    "qr.nspk.ru",
    "t.me",
    "wa.me",
    "api.whatsapp.com",
    "sandbox.pay.yandex.ru",
    "pay.yandex.ru",
    "sso.passport.yandex.ru",
    "passport.yandex.ru",
    "sso.ya.ru",
    "ya.ru",
    "js.stripe.com",
    "m.stripe.network",
    "securepay.tinkoff.ru",
    "www.cdn-tinkoff.ru",
    "cfg.tinkoff.ru",
    "izipoint.by",
    "maxcdn.bootstrapcdn.com",
    "o4504202079502336.ingest.sentry.io",
    "csp.withgoogle.com",
]

// auth origins will open in modal and show toolbar for back into the main origin.
// These should also appear in Info.plist
let authOrigins: [String] = []
// allowedOrigins + authOrigins <= 10

let platformCookie = Cookie(name: "app-platform", value: "iOS App Store")

// UI options
let displayMode = "standalone" // standalone / fullscreen.
let adaptiveUIStyle = true     // iOS 15+ only. Change app theme on the fly to dark/light related to WebView background color.
let overrideStatusBar = false   // iOS 13-14 only. if you don't support dark/light system theme.
let statusBarTheme = "dark"    // dark / light, related to override option.
let pullToRefresh = true    // Enable/disable pull down to refresh page

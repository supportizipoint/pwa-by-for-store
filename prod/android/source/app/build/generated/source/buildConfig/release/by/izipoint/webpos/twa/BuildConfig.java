/**
 * Automatically generated file. DO NOT MODIFY
 */
package by.izipoint.webpos.twa;

public final class BuildConfig {
  public static final boolean DEBUG = false;
  public static final String APPLICATION_ID = "by.izipoint.webpos.twa";
  public static final String BUILD_TYPE = "release";
  public static final int VERSION_CODE = 1;
  public static final String VERSION_NAME = "1.0.0.0";
}
